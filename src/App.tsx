/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useMemo, useCallback} from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableHighlight,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import ComponentsBasicScreen from './screens/baseComponents';
import FormsScreen from './screens/formik/Forms';
import StorageScreen from './screens/asyncStorage';
import FlatListScreen from './screens/flatList';
import ReduxScreen from './screens/redux';

import {store, persistor} from './redux/store';

const linksExamples = [
  {
    title: 'Components Basic',
    link: 'ComponentsBasic',
  },
  {
    title: 'FlatList',
    link: 'FlatList',
  },
  {
    title: 'Formik',
    link: 'Formik',
  },
  {
    title: 'Storage',
    link: 'Storage',
  },
  {
    title: 'Redux',
    link: 'Redux',
  },
];

const Card = ({link, title, navigate}) => {
  const handlerOnPress = useCallback(() => navigate(link), [link, navigate]);
  return (
    <TouchableHighlight
      onPress={handlerOnPress}
      style={styles.cardsContainerButton}>
      <View style={styles.cardsContainer}>
        <Text style={styles.cardsText}>{title}</Text>
        <Text style={[styles.cardsText, styles.cardsTextBorder]}>{'>'}</Text>
      </View>
    </TouchableHighlight>
  );
};

function HomeScreen({navigation}): F {
  const listCards = useMemo(() => {
    return linksExamples.map(link => {
      return (
        <Card
          link={link.link}
          title={link.title}
          navigate={navigation.navigate}
        />
      );
    });
  }, [navigation]);

  return <ScrollView style={styles.container}>{listCards}</ScrollView>;
}

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen
              name="ComponentsBasic"
              component={ComponentsBasicScreen}
            />
            <Stack.Screen name="FlatList" component={FlatListScreen} />
            <Stack.Screen name="Formik" component={FormsScreen} />
            <Stack.Screen name="Storage" component={StorageScreen} />
            <Stack.Screen name="Redux" component={ReduxScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 20,
  },
  highlight: {
    fontWeight: '700',
  },
  cardsContainerButton: {
    borderRadius: 15,
    marginBottom: 15,
  },
  cardsContainer: {
    minWidth: '48%',
    minHeight: 70,
    shadowColor: 'black',
    padding: 15,
    backgroundColor: 'white',
    borderRadius: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  cardsText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  cardsTextBorder: {
    borderLeftWidth: StyleSheet.hairlineWidth,
    borderLeftColor: 'black',
    paddingTop: 10,
    paddingBottom: 10,
  },
});

export default App;
