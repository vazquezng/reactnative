import React from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';

import {useAppSelector, useAppDispatch} from '../../redux/hooks';
import {addItem, deleteItem, checkTodo} from '../../redux/todoListReducer';

import AppBar from './AppBar';
import Todo from './Todo';
import TodoList from './TodoList';

function TodoListRedux() {
  // Initalize empty array to store todos
  // const [todos, setTodos] = useState([]);
  const todos = useAppSelector(state => state.list);
  const dispatch = useAppDispatch();

  // function to add todo object in todo list
  const addTodo = (name: string) => {
    if (name.length > 0) {
      dispatch(addItem({id: Date.now().toString(), name, isChecked: false}));
    }
  };

  // function to mark todo as checked or unchecked
  const handlerCheckTodo = (id: string) => {
    dispatch(checkTodo({id}));
  };

  // function to delete todo from the todo list
  const handlerDeleteItem = (id: string) => {
    dispatch(deleteItem({id}));
  };

  return (
    <View style={styles.container}>
      <View style={styles.statusBar} />
      <AppBar />
      <Todo addTodo={addTodo} />
      <ScrollView>
        {todos.map(todo => (
          <TodoList
            key={todo.id}
            todo={todo}
            checkTodo={handlerCheckTodo}
            deleteTodo={handlerDeleteItem}
          />
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  statusBar: {
    backgroundColor: '#7F39FB',
    color: '#fff',
    width: '100%',
    height: 30,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  todo: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbox: {
    borderWidth: 1,
    borderColor: '#7F39FB',
    borderRadius: 8,
    padding: 10,
    margin: 10,
    width: '80%',
  },
});

export default TodoListRedux;
