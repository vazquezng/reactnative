import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import ImageExample from './image';
import TextExample from './text';
import LayoutExample from './layout';
const Tab = createMaterialTopTabNavigator();

function ComponentsBasicTab() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="TextExample" component={TextExample} />
      <Tab.Screen name="ImageExample" component={ImageExample} />
      <Tab.Screen name="LayoutExample" component={LayoutExample} />
    </Tab.Navigator>
  );
}

export default ComponentsBasicTab;
