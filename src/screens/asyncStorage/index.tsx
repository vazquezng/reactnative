import React, {useState, useEffect} from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import AppBar from './AppBar';
import Todo from './Todo';
import TodoList from './TodoList';

function TodoListAsyncStorage() {
  // Initalize empty array to store todos
  const [todos, setTodos] = useState([]);

  // function to add todo object in todo list
  const addTodo = ({title}) => {
    if (title.length > 0) {
      // Add todo to the list
      setTodos([...todos, {key: Date.now(), name: title, isChecked: false}]);
      // clear the value of the textfield
    }
  };

  // function to mark todo as checked or unchecked
  const checkTodo = id => {
    // loop through todo list and look for the the todo that matches the given id param
    // update the state using setTodos function
    setTodos(
      todos.map(todo => {
        if (todo.key === id) {
          todo.isChecked = !todo.isChecked;
        }
        return todo;
      }),
    );
  };

  // function to delete todo from the todo list
  const deleteTodo = id => {
    // loop through todo list and return todos that don't match the id
    // update the state using setTodos function
    setTodos(
      todos.filter(todo => {
        return todo.key !== id;
      }),
    );
  };

  const storeData = async values => {
    try {
      console.log('storeData', values);
      const jsonValue = JSON.stringify(values);
      await AsyncStorage.setItem('@todoList', jsonValue);
    } catch (e) {
      console.warning(e);
    }
  };

  useEffect(() => {
    console.log(todos.length, 'TodoList length');
    todos.length && storeData(todos);
  }, [todos]);

  useEffect(() => {
    const getData = async () => {
      try {
        const value = await AsyncStorage.getItem('@todoList');
        console.log('getData', value);
        if (value !== null) {
          setTodos(JSON.parse(value));
        }
      } catch (e) {
        console.log(e);
      }
    };
    getData();
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.statusBar} />
      <AppBar />
      <Todo addTodo={addTodo} />
      <ScrollView>
        {todos.map(todo => (
          <TodoList
            key={todo.key}
            todo={todo}
            checkTodo={checkTodo}
            deleteTodo={deleteTodo}
          />
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  statusBar: {
    backgroundColor: '#7F39FB',
    color: '#fff',
    width: '100%',
    height: 30,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  todo: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbox: {
    borderWidth: 1,
    borderColor: '#7F39FB',
    borderRadius: 8,
    padding: 10,
    margin: 10,
    width: '80%',
  },
});

export default TodoListAsyncStorage;
