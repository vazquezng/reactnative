import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import type {RootState} from './store';

// Define a type for the slice state
interface TodoListItemState {
  id: string;
  name: string;
  isChecked: boolean;
}
interface TodoListState {
  list: TodoListItemState[];
}

// Define the initial state using that type
const initialState: TodoListState = {
  list: [],
};

export const todoListSlice = createSlice({
  name: 'todoList',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    // Use the PayloadAction type to declare the contents of `action.payload`
    addItem: (state: TodoListState, action: PayloadAction<any>) => {
      state.list.push(action.payload);
    },
    deleteItem: (state: TodoListState, action: PayloadAction<any>) => {
      state.list = state.list.filter((todo: any) => {
        return todo.id !== action.payload.id;
      });
    },
    checkTodo: (state: TodoListState, action: PayloadAction<any>) => {
      state.list.map((todo: any) => {
        if (todo.id === action.payload.id) {
          todo.isChecked = !todo.isChecked;
        }
        return todo;
      });
    },
  },
});

export const {addItem, deleteItem, checkTodo} = todoListSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectList = (state: RootState) => state.todoList.list;

export default todoListSlice.reducer;
